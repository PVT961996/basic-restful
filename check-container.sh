#!/bin/bash
result=$(sudo docker images -q basic-restful)

if [[ -n "$result" ]]; then
	  echo "Container exists"
	  sudo docker container rm --force thanhpv
else
	  echo "No such container"
fi
name="pvt961996/basic-restful"
localimg="basic-restful"
tag=$(git log -1 --pretty=%H)
img="$localimg:$tag"
lastest="$name:latest"
sudo docker build -t $img .
sudo docker tag $img $lastest 
sudo docker login -u ${DOCKER_USER} -p ${DOCKER_PASS}
sudo docker push $lastest
sudo docker run -d -p 9000:80 --name thanhpv $img
